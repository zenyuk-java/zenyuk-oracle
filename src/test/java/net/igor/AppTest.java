package net.igor;

import static org.junit.jupiter.api.Assertions.assertEquals;

import net.igor.cleaningsimulator.site.BlockType;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class AppTest
{
    @Test
    public void Given_FileLines_When_ConvertToSequence_Then_GetCorrectOrder() {
        // arrange
        List<String> site = Arrays.asList("oot","oTo");
        List<BlockType> expected = Arrays.asList(
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND,
                BlockType.REMOVABLE_TREE,
                BlockType.PLAIN_LAND,
                BlockType.PRESERVE_TREE,
                BlockType.PLAIN_LAND);

        // act
        List<BlockType> result = App.sequelize(site);

        // assert
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), result.get(i));
        }
    }
}
