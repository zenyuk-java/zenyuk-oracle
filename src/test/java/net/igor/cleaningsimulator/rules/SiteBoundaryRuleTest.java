package net.igor.cleaningsimulator.rules;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SiteBoundaryRuleTest {
    @Test
    public void Given_PositionOnSite_When_RuleIsChecked_Then_CheckPasses() {
        // arrange
        Rule rule = new SiteBoundaryRule(2, 2);

        // act
        boolean result = rule.check(0, 0);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void Given_PositionOffSiteToSouth_When_RuleIsChecked_Then_CheckFails() {
        // arrange
        Rule rule = new SiteBoundaryRule(2, 2);

        // act
        boolean result = rule.check(0, 2);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void Given_PositionOffSiteToWest_When_RuleIsChecked_Then_CheckFails() {
        // arrange
        Rule rule = new SiteBoundaryRule(2, 2);

        // act
        boolean result = rule.check(-1, 1);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void Given_PositionOffSiteToNorth_When_RuleIsChecked_Then_CheckFails() {
        // arrange
        Rule rule = new SiteBoundaryRule(2, 2);

        // act
        boolean result = rule.check(0, -1);

        // assert
        assertEquals(false, result);
    }

    @Test
    public void Given_PositionOffSiteToEast_When_RuleIsChecked_Then_CheckFails() {
        // arrange
        Rule rule = new SiteBoundaryRule(2, 2);

        // act
        boolean result = rule.check(2, 1);

        // assert
        assertEquals(false, result);
    }
}
