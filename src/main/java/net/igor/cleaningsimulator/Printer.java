package net.igor.cleaningsimulator;

import java.util.List;

public class Printer {
    public void printSummary(List<String> commandsHistory, int fuelUsage, int bulldozerDamage, int siteDamage,
                                     long unclearedBlocks) {
        int communicationOverhead = commandsHistory.size() - 1;
        long totalCost = communicationOverhead
                + fuelUsage
                + unclearedBlocks * 3
                + siteDamage * 10
                + bulldozerDamage * 2;
        System.out.printf("The simulation has ended at your request. These are the commands you issued:\n\n");
        System.out.println(String.join(", ", commandsHistory) + "\n");
        System.out.printf("The costs for this land clearing operation were:\n\n");
        System.out.println("Item                            Quantity    \tCost");
        System.out.println("communication overhead                 " + communicationOverhead + "      \t" + communicationOverhead);
        System.out.println("fuel usage                             " + fuelUsage + "      \t" + fuelUsage);
        System.out.println("uncleared squares                      " + unclearedBlocks + "      \t" + unclearedBlocks * 3);
        System.out.println("destruction of protected tree          " + siteDamage + "      \t" + siteDamage * 10);
        System.out.println("paint damage to bulldozer              " + bulldozerDamage + "      \t" + bulldozerDamage * 2);
        System.out.println("---");
        System.out.println("Total                                         \t" + totalCost);
        System.out.println("Thank you for using the Aconex site clearing simulator.");
    }
}
