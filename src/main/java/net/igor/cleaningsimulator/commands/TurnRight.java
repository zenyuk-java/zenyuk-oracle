package net.igor.cleaningsimulator.commands;

import net.igor.cleaningsimulator.machinery.Bulldozer;

public class TurnRight implements Command {
    private Bulldozer bulldozer;

    public TurnRight(Bulldozer bulldozer) {
        this.bulldozer = bulldozer;
    }

    @Override
    public void execute() {
        bulldozer.turnRight();
    }

    @Override
    public char getShortCode() {
        return 'r';
    }
}
