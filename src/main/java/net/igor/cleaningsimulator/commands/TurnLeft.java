package net.igor.cleaningsimulator.commands;

import net.igor.cleaningsimulator.machinery.Bulldozer;

public class TurnLeft implements Command {
    private Bulldozer bulldozer;

    public TurnLeft(Bulldozer bulldozer) {
        this.bulldozer = bulldozer;
    }

    @Override
    public void execute() {
        bulldozer.turnLeft();
    }

    @Override
    public char getShortCode() {
        return 'l';
    }
}
