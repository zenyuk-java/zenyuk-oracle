package net.igor.cleaningsimulator.commands;

import net.igor.cleaningsimulator.machinery.Bulldozer;

public class Quit implements Command {
    private Bulldozer bulldozer;

    public Quit(Bulldozer bulldozer) {
        this.bulldozer = bulldozer;
    }

    @Override
    public void execute() {
        bulldozer.addQuitCommandHistory();
    }

    @Override
    public char getShortCode() {
        return 'q';
    }
}
