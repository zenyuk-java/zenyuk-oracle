package net.igor.cleaningsimulator.site;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SiteTest {
    @Test
    public void Given_CreatingSite_When_InvalidBlocksProvided_ThenThrowException() {
        // arrange
        int siteWidth = 2;
        int siteLength = 2;
        List<BlockType> invalidBlocks = Arrays.asList(BlockType.PLAIN_LAND);

        // act, assert
        assertThrows(IllegalArgumentException.class, () ->
                new Site(siteWidth, siteLength, invalidBlocks));
    }

    @Test
    public void Given_Site3By5_When_GetBlockBy2DIndex_Then_CorrectSequentialIdxReturned() {
        // arrange
        int width = 3;
        int widthIndex = 1;
        int lengthIndex = 4;

        // act
        int sequentialIndex = Site.twoDimensionalIndexToSequential(width, widthIndex, lengthIndex);

        // assert
        assertEquals(13, sequentialIndex);
    }

    @Test
    public void Given_TwoSmallIntCoordinates_When_Combined_Then_ValidLongResult() {
        // arrange
        long expected = 4294967297l;

        // act
        long result = Site.coordinatesToCombinedKey(1,1);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void Given_TwoMaxIntCoordinates_When_Combined_Then_ValidLongResult() {
        // arrange
        long expected = 9223372034707292159l;

        // act
        long result = Site.coordinatesToCombinedKey(Integer.MAX_VALUE, Integer.MAX_VALUE);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void Given_MaxIntAndZeroIntCoordinates_When_Combined_Then_ValidLongResult() {
        // arrange
        long expected = 9223372032559808512l;

        // act
        long result = Site.coordinatesToCombinedKey(Integer.MAX_VALUE, 0);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void Given_TwoZeroIntCoordinates_When_Combined_Then_ValidLongResult() {
        // arrange
        long expected = 0;

        // act
        long result = Site.coordinatesToCombinedKey(0, 0);

        // assert
        assertEquals(expected, result);
    }

    @Test
    public void Given_ZeroAndMaxIntCoordinates_When_Combined_Then_ValidLongResult() {
        // arrange
        long expected = 2147483647l;

        // act
        long result = Site.coordinatesToCombinedKey(0, Integer.MAX_VALUE);

        // assert
        assertEquals(expected, result);
    }
}
