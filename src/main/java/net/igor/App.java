package net.igor;

import net.igor.cleaningsimulator.Printer;
import net.igor.cleaningsimulator.site.BlockType;
import net.igor.cleaningsimulator.machinery.Bulldozer;
import net.igor.cleaningsimulator.site.Site;
import net.igor.cleaningsimulator.commands.*;
import net.igor.cleaningsimulator.rules.PreserveTreeRule;
import net.igor.cleaningsimulator.rules.Rule;
import net.igor.cleaningsimulator.rules.SiteBoundaryRule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Specify a Site file");
            System.exit(1);
        }

        String siteFilePath = args[0];
        Site site = readSite(siteFilePath);
        Bulldozer bulldozer = new Bulldozer(site);

        List<Rule> rules = Arrays.asList(
                new PreserveTreeRule(site),
                new SiteBoundaryRule(site.westToEastLength(), site.northToSouthLength()));

        List<Command> commands = Arrays.asList(
                new Advance(bulldozer),
                new Quit(bulldozer),
                new TurnLeft(bulldozer),
                new TurnRight(bulldozer));

        Printer printer = new Printer();
        Scanner scanner = new Scanner(System.in);
        listenAndAct(scanner, commands, rules, bulldozer);

        printer.printSummary(bulldozer.getCommandHistory(), bulldozer.getFuelUsage(), bulldozer.getDamage(),
                site.getDamage(), site.countUnclearedBlocks());
    }

    static Command parseCommand(String commandRaw, List<Command> commands) {
        char shortCommandCode = commandRaw.charAt(0);
        Command result = null;
        for (Command c : commands) {
            if (c.getShortCode() == shortCommandCode) {
                result = c;
                if (c instanceof Advance) {
                    int stepsCount = Integer.parseInt(commandRaw.substring(2));
                    ((Advance) c).setStepsCount(stepsCount);
                }
            }
        }
        return result;
    }

    static List<BlockType> sequelize(List<String> input) {
        List<BlockType> result = new ArrayList<>();
        for (String s : input) {
            for (int i = 0; i < s.length(); i++) {
                result.add(BlockType.convertToBlockType(s.charAt(i)));
            }
        }
        return result;
    }

    private static void listenAndAct(Scanner scanner, List<Command> commands, List<Rule> rules, Bulldozer bulldozer) {
        Command command;
        do {
            System.out.print("(l)eft, (r)ight, (a)dvance <n>, (q)uit: ");
            String commandRaw = scanner.nextLine();
            command = parseCommand(commandRaw, commands);
            command.execute();
            // for case when quiting without any moves
            if (bulldozer.getCommandHistory().size() > 1) {
                boolean rulesCheckPassed = checkRules(rules, bulldozer.getPositionWestEast(), bulldozer.getPositionNorthSouth());
                if (!rulesCheckPassed) {
                    break;
                }
            }
        } while (!(command instanceof Quit));
    }

    static boolean checkRules(List<Rule> rules, int positionWestEast, int positionNorthSouth) {
        for (Rule r : rules) {
            if (!r.check(positionWestEast, positionNorthSouth)) {
                System.out.println("Rule was broken: " + r.toString());
                return false;
            }
        }
        return true;
    }

    private static Site readSite(String siteFilePath) {
        List<String> fileLines = new ArrayList<>();
        try {
            fileLines = Files.readAllLines(Paths.get(siteFilePath));
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error reading file " + siteFilePath);
            System.exit(1);
        }

        List<String> linesStripped = new ArrayList<>();
        for (String s : fileLines) {
          linesStripped.add(s.replace(" ", ""));
        }
        if (!isValidFileInput(linesStripped)) {
            System.out.println("Invalid Site File");
        }

        int northToSouthLength = linesStripped.size();
        int westToEastLength = linesStripped.get(0).length();
        List<BlockType> blocksSequence = sequelize(linesStripped);
        return new Site(westToEastLength, northToSouthLength, blocksSequence);
    }

    private static boolean isValidFileInput(List<String> input) {
        int length = input.get(0).length();
        for (String s : input) {
            if (s.length() != length) {
                return false;
            }
        }
        return true;
    }
}
