package net.igor.cleaningsimulator.site;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BlockTypeTest {
    @Test
    public void Given_CharForATree_When_ConvertToBlockType_Then_ResultInCorrectType() {
        // arrange
        char aTreeBlock = 't';

        // act
        BlockType blockType = BlockType.convertToBlockType(aTreeBlock);

        // assert
        assertEquals(BlockType.REMOVABLE_TREE, blockType);
    }
}
