package net.igor.cleaningsimulator.site;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Site {
    private Map<Long, Block> blocks;
    private int westToEastLength;
    private int northToSouthLength;
    private int damage;

    public Site(int westToEastLength, int northToSouthLength, List<BlockType> blocksSequence) {
        if (blocksSequence.size() != westToEastLength * northToSouthLength) {
            throw new IllegalArgumentException("blocks count don't match width and length");
        }

        this.westToEastLength = westToEastLength;
        this.northToSouthLength = northToSouthLength;
        damage = 0;

        blocks = new HashMap<>(westToEastLength * northToSouthLength);
        for (int n = 0; n < northToSouthLength; n++) {
            for (int w = 0; w < westToEastLength; w++) {
                int sequentialIndex = twoDimensionalIndexToSequential(westToEastLength, w, n);
                BlockType blockType = blocksSequence.get(sequentialIndex);
                Block block = new Block(blockType, n, w);

                Long key = coordinatesToCombinedKey(w, n);
                blocks.put(key, block);
            }
        }
    }

    public Block getBlock(int positionWestEast, int positionNorthSouth) {
        Long key = coordinatesToCombinedKey(positionWestEast, positionNorthSouth);
        return blocks.get(key);
    }

    public int westToEastLength() {
        return westToEastLength;
    }

    public int northToSouthLength() {
        return northToSouthLength;
    }

    public void setDestructionOfProtectedTreeDamage() {
        damage++;
    }

    public int getDamage() {
        return damage;
    }

    static int twoDimensionalIndexToSequential(int width, int widthIndex, int lengthIndex) {
        return lengthIndex * width + widthIndex;
    }

    static Long coordinatesToCombinedKey(int positionWestEast, int positionNorthSouth) {
        Long result = ((long) positionWestEast) << 32;
        result += (long) positionNorthSouth;
        return result;
    }

    public long countUnclearedBlocks() {
        return blocks.values().stream()
                .filter(b -> !b.isCleaned() && (BlockType.PLAIN_LAND == b.getBlockType()
                        || BlockType.REMOVABLE_TREE == b.getBlockType()
                        || BlockType.ROCKY_LAND == b.getBlockType()))
                .count();
    }
}
