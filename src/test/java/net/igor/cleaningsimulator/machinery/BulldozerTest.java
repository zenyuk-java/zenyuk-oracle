package net.igor.cleaningsimulator.machinery;

import net.igor.cleaningsimulator.site.BlockType;
import net.igor.cleaningsimulator.site.Site;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BulldozerTest {
    @Test
    public void Given_DefaultPosition_When_AdvancedASingleBlock_Then_PositionIsCorrect() {
        // arrange
        Site site = new Site(2, 2, Arrays.asList(
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);

        // act
        bulldozer.advance(1);

        // assert
        int westEastPosition = bulldozer.getPositionWestEast();
        int northSouthPosition = bulldozer.getPositionNorthSouth();
        assertEquals(0, westEastPosition);
        assertEquals(0, northSouthPosition);
    }

    @Test
    public void Given_AnyPosition_When_AdvanceWithNegativeCount_Then_BulldozerDoesntMove() {
        // arrange
        Site site = new Site(2, 2, Arrays.asList(
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND,
                BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);

        // act
        bulldozer.advance(-2);

        // assert
        int westEastPosition = bulldozer.getPositionWestEast();
        int northSouthPosition = bulldozer.getPositionNorthSouth();
        assertEquals(0, westEastPosition);
        assertEquals(0, northSouthPosition);
    }

    @Test
    public void Given_BulldozerFacingEast_When_TurnedToRight_Then_FacingSouth() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);

        // act
        bulldozer.turnRight();

        // assert
        assertEquals(Direction.SOUTH, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingEast_When_TurnedToLeft_Then_FacingNorth() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);

        // act
        bulldozer.turnLeft();

        // assert
        assertEquals(Direction.NORTH, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingSouth_When_TurnedToRight_Then_FacingWest() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        bulldozer.turnRight();

        // act
        bulldozer.turnRight();

        // assert
        assertEquals(Direction.WEST, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingSouth_When_TurnedToLeft_Then_FacingEast() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        bulldozer.turnRight();

        // act
        bulldozer.turnLeft();

        // assert
        assertEquals(Direction.EAST, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingWest_When_TurnedToRight_Then_FacingNorth() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        bulldozer.turnRight();
        bulldozer.turnRight();

        // act
        bulldozer.turnRight();

        // assert
        assertEquals(Direction.NORTH, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingWest_When_TurnedToLeft_Then_FacingSouth() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        bulldozer.turnLeft();
        bulldozer.turnLeft();

        // act
        bulldozer.turnLeft();

        // assert
        assertEquals(Direction.SOUTH, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingNorth_When_TurnedToRight_Then_FacingEast() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        bulldozer.turnLeft();

        // act
        bulldozer.turnRight();

        // assert
        assertEquals(Direction.EAST, bulldozer.getDirection());
    }

    @Test
    public void Given_BulldozerFacingNorth_When_TurnedToLeft_Then_FacingWest() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Bulldozer bulldozer = new Bulldozer(site);
        bulldozer.advance(1);
        bulldozer.turnLeft();

        // act
        bulldozer.turnLeft();

        // assert
        assertEquals(Direction.WEST, bulldozer.getDirection());
    }
}
