package net.igor.cleaningsimulator.site;

public enum BlockType {
    PLAIN_LAND,
    ROCKY_LAND,
    REMOVABLE_TREE,
    PRESERVE_TREE;

    public static BlockType convertToBlockType(char rawBlock) {
        switch (rawBlock) {
            case 'o' : return PLAIN_LAND;
            case 'r' : return ROCKY_LAND;
            case 't' : return REMOVABLE_TREE;
            case 'T' : return PRESERVE_TREE;
            default: throw new UnsupportedOperationException("Invalid Block Type" + rawBlock);
        }
    }
}
