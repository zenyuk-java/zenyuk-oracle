package net.igor.cleaningsimulator.machinery;

public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
