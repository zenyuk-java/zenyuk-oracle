package net.igor.cleaningsimulator.rules;

public class SiteBoundaryRule implements Rule {
    private int westToEastLength;
    private int northToSouthLength;

    public SiteBoundaryRule(int westToEastLength, int northToSouthLength) {
        this.westToEastLength = westToEastLength;
        this.northToSouthLength = northToSouthLength;
    }

    @Override
    public boolean check(int positionWestEast, int positionNorthSouth) {
        if (positionWestEast >= westToEastLength
                || positionNorthSouth >= northToSouthLength
                || positionWestEast < 0
                || positionNorthSouth < 0) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SiteBoundaryRule - An attempt to leave site's boundaries";
    }
}
