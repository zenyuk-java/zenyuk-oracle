package net.igor.cleaningsimulator.machinery;

import net.igor.cleaningsimulator.site.Block;
import net.igor.cleaningsimulator.site.BlockType;
import net.igor.cleaningsimulator.site.Site;

import java.util.ArrayList;
import java.util.List;

public class Bulldozer {
    private final Site site;
    private Direction direction;
    private List<String> commandHistory;

    // positionWestEast - zero-based position from left to right
    private int positionWestEast;

    // positionNorthSouth - zero-based position from top to bottom
    private int positionNorthSouth;
    private int fuelUsage;
    private int damage;

    public Bulldozer(Site site) {
        this.site = site;
        fuelUsage = 0;
        damage = 0;
        commandHistory = new ArrayList<>();
        direction = Direction.EAST;
        positionWestEast = -1;
        positionNorthSouth = 0;
    }

    public Direction getDirection() {
        return direction;
    }

    public void advance(int count) {
        if (count < 1) {
            System.out.println("Error - count of block to advance should be 1 or more");
            return;
        }

        for (int i = 0; i < count; i++) {
            boolean moreStepsLeft = count - i > 1;
            advance(moreStepsLeft);
        }
        commandHistory.add("advance " + count);
    }

    public void turnLeft() {
        switch (direction) {
            case EAST:
                direction = Direction.NORTH;
                break;
            case SOUTH:
                direction = Direction.EAST;
                break;
            case WEST:
                direction = Direction.SOUTH;
                break;
            case NORTH:
                direction = Direction.WEST;
                break;
            default:
                System.out.println("Error - Unknown current direction");
                System.exit(1);
        }
        commandHistory.add("turn left");
    }

    public void turnRight() {
        switch (direction) {
            case EAST:
                direction = Direction.SOUTH;
                break;
            case SOUTH:
                direction = Direction.WEST;
                break;
            case WEST:
                direction = Direction.NORTH;
                break;
            case NORTH:
                direction = Direction.EAST;
                break;
            default:
                System.out.println("Error - Unknown current direction");
                System.exit(1);
        }
        commandHistory.add("turn right");
    }

    public int getPositionWestEast() {
        return positionWestEast;
    }

    public int getPositionNorthSouth() {
        return positionNorthSouth;
    }

    public int getDamage() {
        return damage;
    }

    private void advance(boolean moreStepsLeft) {
        switch (direction) {
            case EAST:
                positionWestEast += 1;
                break;
            case SOUTH:
                positionNorthSouth += 1;
                break;
            case WEST:
                positionWestEast -= 1;
                break;
            case NORTH:
                positionNorthSouth -= 1;
                break;
            default:
                System.out.println("Error - Unknown direction to advance");
        }
        fuelUsage++;
        Block currentBlock = site.getBlock(positionWestEast, positionNorthSouth);
        if (BlockType.PRESERVE_TREE == currentBlock.getBlockType()) {
            site.setDestructionOfProtectedTreeDamage();
        }
        if (!currentBlock.isCleaned()
                && (BlockType.ROCKY_LAND == currentBlock.getBlockType()
                || BlockType.REMOVABLE_TREE == currentBlock.getBlockType())) {
            fuelUsage++;
        }
        if (moreStepsLeft && BlockType.REMOVABLE_TREE == currentBlock.getBlockType()) {
            damage++;
        }
        currentBlock.setCleaned();
    }

    public List<String> getCommandHistory() {
        return commandHistory;
    }

    public void addQuitCommandHistory() {
        commandHistory.add("quit");
    }

    public int getFuelUsage() {
        return fuelUsage;
    }
}
