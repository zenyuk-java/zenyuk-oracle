package net.igor.cleaningsimulator.commands;

public interface Command {
    void execute();
    char getShortCode();
}
