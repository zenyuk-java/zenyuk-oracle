package net.igor.cleaningsimulator.rules;

public interface Rule {
    boolean check(int positionWestEast, int positionNorthSouth);
}
