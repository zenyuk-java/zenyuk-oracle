package net.igor.cleaningsimulator.rules;

import net.igor.cleaningsimulator.site.BlockType;
import net.igor.cleaningsimulator.site.Site;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PreserveTreeRuleTest {
    @Test
    public void Given_PlainLandBlock_When_RuleIsChecked_Then_CheckPasses() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PLAIN_LAND));
        Rule rule = new PreserveTreeRule(site);

        // act
        boolean result = rule.check(0, 0);

        // assert
        assertEquals(true, result);
    }

    @Test
    public void Given_PreserveTreeBlock_When_RuleIsChecked_Then_CheckFails() {
        // arrange
        Site site = new Site(1, 1, Arrays.asList(BlockType.PRESERVE_TREE));
        Rule rule = new PreserveTreeRule(site);

        // act
        boolean result = rule.check(0, 0);

        // assert
        assertEquals(false, result);
    }
}
