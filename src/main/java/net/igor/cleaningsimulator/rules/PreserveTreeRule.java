package net.igor.cleaningsimulator.rules;

import net.igor.cleaningsimulator.site.Block;
import net.igor.cleaningsimulator.site.BlockType;
import net.igor.cleaningsimulator.site.Site;

public class PreserveTreeRule implements Rule {
    private Site site;

    public PreserveTreeRule(Site site) {
        this.site = site;
    }

    @Override
    public boolean check(int positionWestEast, int positionNorthSouth) {
        Block block = site.getBlock(positionWestEast, positionNorthSouth);
        if (block == null) {
            throw new IllegalArgumentException("Site position incorrect: " + positionWestEast + " and " + positionNorthSouth);
        }
        if (BlockType.PRESERVE_TREE == block.getBlockType()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "PreserveTreeRule - An attempt to damage a tree that must be preserved";
    }
}
