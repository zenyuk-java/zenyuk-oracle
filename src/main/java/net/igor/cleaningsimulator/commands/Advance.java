package net.igor.cleaningsimulator.commands;

import net.igor.cleaningsimulator.machinery.Bulldozer;

public class Advance implements Command {
    private Bulldozer bulldozer;
    private int stepsCount;

    public Advance(Bulldozer bulldozer) {
        this.bulldozer = bulldozer;
    }

    public void setStepsCount(int stepsCount) {
        this.stepsCount = stepsCount;
    }

    @Override
    public void execute() {
        bulldozer.advance(stepsCount);
    }

    @Override
    public char getShortCode() {
        return 'a';
    }
}
