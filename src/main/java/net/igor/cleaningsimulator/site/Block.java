package net.igor.cleaningsimulator.site;

public class Block {
    private boolean cleaned;
    private BlockType type;

    // positionWestEast - zero-based position from left to right
    private int positionWestEast;

    // positionNorthSouth - zero-based position from top to bottom
    private int positionNorthSouth;

    public Block(BlockType type, int positionNorthSouth, int positionWestEast) {
        cleaned = false;
        this.type = type;
        this.positionNorthSouth = positionNorthSouth;
        this.positionWestEast = positionWestEast;
    }

    public boolean isCleaned() {
        return cleaned;
    }

    public void setCleaned() {
        cleaned = true;
    }

    public BlockType getBlockType() {
        return type;
    }
}
