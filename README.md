# Site Clearing Simulation
To compile and run the application you will need Maven and Java 1.8

How to run:

1. Navigate to the repository root folder (zenyuk-oracle)
2. Run "mvn package"
3. Run "java -cp target/mymaven-1.0-SNAPSHOT.jar net.igor.App input.txt"
4. Follow the on-screen instruction as per the requirement.

Sample file "input.txt" is provided with the solution.

# Design and approach

The source code is organised using typical Maven structure. I was trying to use TDD 
where possible without slowing down productivity too much. You can see test classes 
under test/java/net.igor/.

All tests where named using BDD practice Given-When-Then and Arrange-Act-Assert.

Code is grouped into few main classes. Bulldozer - representing domain of the 
physically moving machine and related attributes. Site is a representation of a
piece of land to be cleaned. Site consists of Blocks. There are different types 
of Blocks, represented by corresponding enum. 

Bulldozer can perform commands, which represented by Command interface and few classes
inheriting it in net.igor.cleaningsimulator.commands package.

Special business requirements are implemented in Rules package, namely PreserveTreeRule
and SiteBoundaryRule.

In the solution, there was some effort put to split domain responsibilities. Like we 
have damages calculated for bulldozer in Bulldozer domain and another type of damage (preserve
tree one) in Site domain.

